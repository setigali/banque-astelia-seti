package fr.afpa.controles;

import java.util.Map.Entry;

import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;

public class ControleExistenceConseiller {

	/**
	 * Verifie si l'id du conseiller passe en parametre existe
	 * dans les listes de conseillers des agences de la banque passee en parametre.
	 * @param banque
	 * @param idConseiller
	 * @return
	 */
	public boolean existenceConseillerDansBanqueParId(Banque banque, String idConseiller) {
		ControleExistenceConseiller cec = new ControleExistenceConseiller();
		for (Entry<String, Agence> agence : banque.getListeAgences().entrySet()) {
			if (cec.existenceConseillerDansAgenceParID(agence.getValue(), idConseiller)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Verifie si l'id du conseiller passe en parametre existe
	 * dans la liste de conseiller de l'agence passee en parametre.
	 * @param agence
	 * @param idConseiller
	 * @return
	 */
	public boolean existenceConseillerDansAgenceParID(Agence agence, String idConseiller) {
		return agence.getListeConseiller().containsKey(idConseiller); 
	}
	
}
