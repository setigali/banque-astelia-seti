package fr.afpa.controles;

import java.util.HashMap;
import java.util.Map.Entry;

import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Conseiller;

public class ControleExistenceCompte {

	public boolean controleExistenceCompteBancaireDansListeDeCompteDuClient(Client client, String compteBancaire) {

		return client.getTableauDeCompteBancaire().containsKey(compteBancaire);

	}

	public boolean controleExistenceCompteBancaireDansConseiller(Conseiller conseiller, String compteBancaire) {

		for (Entry<String, Client> client : conseiller.getListeClients().entrySet()) {
			ControleExistenceCompte cec = new ControleExistenceCompte();
			if (cec.controleExistenceCompteBancaireDansListeDeCompteDuClient(client.getValue(), compteBancaire)) {
				return true;
			}

		}

		return false;
	}

	public boolean controleExistenceCompteBancaireDansAgence(Agence agence, String compteBancaire) {
		for (Entry<String, Conseiller> conseiller : agence.getListeConseiller().entrySet()) {
			ControleExistenceCompte cec = new ControleExistenceCompte();
			if (cec.controleExistenceCompteBancaireDansConseiller(conseiller.getValue(), compteBancaire)) {
				return true;

			}

		}
		return false;
	}

	public boolean controleExistenceCompteBancaireDansBanque(Banque banque, String compteBancaire) {
		for (Entry<String, Agence> agence : banque.getListeAgences().entrySet()) {
			ControleExistenceCompte cec = new ControleExistenceCompte();
			if (cec.controleExistenceCompteBancaireDansAgence(agence.getValue(), compteBancaire)) {
				return true;
			}

		}
		return false;
	}
}
