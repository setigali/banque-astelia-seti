package fr.afpa.controles;

import java.util.Map;
import java.util.Map.Entry;

import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Conseiller;

public class ControleExistenceClient {

	/**
	 * Verifie si l'id du client passe en parametre est
	 * bien dans la liste du conseiller passe en parametre.
	 * @param conseiller
	 * @param idClient
	 * @return
	 */
	public boolean existenceClientDansConseillerParID(Conseiller conseiller, String idClient) {
		return conseiller.getListeClients().containsKey(idClient);
	}
	
	/**
	 * Verifie si le nom et le prenom passes en parametre correspondent a 
	 * un client dans la liste de client du conseiller passe en parametre.
	 * @param conseiller
	 * @param nom
	 * @param prenom
	 * @return
	 */
	public boolean existenceClientDansConseillerParNomPrenom(Conseiller conseiller, String nom, String prenom) {
		Map<String, Client> listeClients = conseiller.getListeClients();
		for (Entry<String, Client> client : listeClients.entrySet()) {
			if (client.getValue().getNom().equals(nom) && client.getValue().getPrenom().equals(prenom)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Verifie si l'id passe en parametre correspond a un client qui
	 * se trouve dans l'une des listes de conseiller de l'agence passee en parametre. 
	 * La fonction retourne vrai si elle le trouve et faux sinon.
	 * @param agence
	 * @param idClient
	 * @return
	 */
	public boolean existenceClientDansAgenceParID(Agence agence, String idClient) {
		for (Entry<String, Conseiller> conseiller : agence.getListeConseiller().entrySet()) {
			ControleExistenceClient cec = new ControleExistenceClient();
			if (cec.existenceClientDansConseillerParID(conseiller.getValue(), idClient)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Verifie si le nom et le prenom passes en parametre correspondent a un client qui
	 * se trouve dans l'une des listes de conseiller de l'agence passee en parametre. 
	 * La fonction retourne vrai si elle le trouve et faux sinon.
	 * @param agence
	 * @param nom
	 * @param prenom
	 * @return
	 */
	public boolean existenceClientDansAgenceParNomPrenom(Agence agence, String nom, String prenom) {
		for (Entry<String, Conseiller> conseiller : agence.getListeConseiller().entrySet()) {
			ControleExistenceClient cec = new ControleExistenceClient();
			if (cec.existenceClientDansConseillerParNomPrenom(conseiller.getValue(), nom, prenom)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Verifie si l'id passe en parametre correspond a un client qui
	 * se trouve dans l'une des listes de conseillers des agences de la banque passee 
	 * en parametre. La fonction retourne vrai si elle le trouve et faux sinon.
	 * @param banque
	 * @param idClient
	 * @return
	 */
	public boolean existenceClientDansBanqueParID(Banque banque, String idClient) {
		for (Entry<String, Agence> agence : banque.getListeAgences().entrySet()) {
			ControleExistenceClient cec = new ControleExistenceClient();
			if (cec.existenceClientDansAgenceParID(agence.getValue(), idClient)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Verifie si le nom et le prenom passes en parametre correspondent a un client qui
	 * se trouve dans l'une des listes de conseillers des agences de la banque passee en parametre. 
	 * La fonction retourne vrai si elle le trouve et faux sinon.
	 * @param banque
	 * @param nom
	 * @param prenom
	 * @return
	 */
	public boolean existenceClientDansBanqueParNomPrenom(Banque banque, String nom, String prenom) {
		for (Entry<String, Agence> agence : banque.getListeAgences().entrySet()) {
			ControleExistenceClient cec = new ControleExistenceClient();
			if (cec.existenceClientDansAgenceParNomPrenom(agence.getValue(), nom, prenom)) {
				return true;
			}
		}
		return false;
	}
}
