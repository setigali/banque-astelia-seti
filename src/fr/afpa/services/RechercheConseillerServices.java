package fr.afpa.services;

import java.util.Map.Entry;

import fr.afpa.controles.ControleExistenceConseiller;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Conseiller;

public class RechercheConseillerServices {

	/**
	 * Demande la banque dans la quelle on cherche le conseiller
	 * ainsi que l'id du  et retourne le conseiller recherche.
	 * @param banque
	 * @param id
	 * @return
	 */
	public Conseiller rechercheConseillerDansBanqueParId(Banque banque, String idConseiller) {
		ControleExistenceConseiller cec = new ControleExistenceConseiller();
		for (Entry<String, Agence> agence : banque.getListeAgences().entrySet()) {
			if (cec.existenceConseillerDansAgenceParID(agence.getValue(), idConseiller)) {
				return rechercheConseillerDansAgenceParID(agence.getValue(), idConseiller);
			}
		}
		return null;
	}
	
	/**
	 * Retourne le conseiller dont l'id correspond a l'id passe en parametre
	 * qui se trouve dans l'agence passee en parametre..
	 * S'il ne le trouve pas, retourne null.
	 * @param agence
	 * @param id
	 * @return
	 */
	public Conseiller rechercheConseillerDansAgenceParID(Agence agence, String id) {
		return agence.getListeConseiller().get(id);
	}
	
}
