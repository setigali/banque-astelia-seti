package fr.afpa.services;

import java.util.Random;

import fr.afpa.entite.Client;

public class Aleatoire {
	
	public static String majusculesAleatoire(int nombreBoucle) {
		
		Random aleatoire = new Random();
		String idClient = "";
		for (int i=0;i<nombreBoucle;i++) {
		char majAleatoire = (char)(aleatoire.nextInt(90-65)+65);
		idClient+= majAleatoire;
	}
	return idClient;

	}
	
	/**
	 * Permet de retourner une chaine de caracteres constituee uniquement de chiffres
	 * aleatoires. La longueur de cette chaine est passee en parametre.
	 * @param nombreBoucle
	 * @return
	 */
	public static String chiffresAleatoire(int nombreBoucle) {
		Random aleatoire = new Random();
		String res = "";
		for (int i=0;i<nombreBoucle;i++) {
		char chiffre = (char)(aleatoire.nextInt(57-48)+48);
		res+= chiffre;
	}
	return res;

	}
}