package fr.afpa.services;

import java.util.Map.Entry;

import fr.afpa.controles.ControleExistenceClient;
import fr.afpa.controles.Saisie;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Conseiller;

public class RechercheClientServices {

	/**
	 * Permet de choisir la maniere de chercher le client
	 * et appelle la fonction correspondante.
	 * @param banque
	 * @return
	 */
	public Client rechercheClientParIDOuNom(Banque banque) {
		String idOuNom = Saisie.saisieIdClientOuNomPrenom(banque.getIn());
		if ("id".equals(idOuNom)) {
			return rechercheClientDansBanqueParSaisieID(banque);
		} else if ("nom".equals(idOuNom)) {
			return rechercheClientDansBanqueParSaisieNomPrenom(banque);
		}
		return null;
	}

	/**
	 * Permet de saisir le nom et le prenom du client a recherche et le retourne s'il
	 * est trouve. Retourne null sinon.
	 * @param banque
	 * @return
	 */
	public Client rechercheClientDansBanqueParSaisieNomPrenom(Banque banque) {
		System.out.println("Veuillez entrer le nom du client");
		String nom = Saisie.saisieVide(banque.getIn());
		System.out.println("Veuillez entrer le prenom du client");
		String prenom = Saisie.saisieVide(banque.getIn());

		ControleExistenceClient cec = new ControleExistenceClient();
		if (cec.existenceClientDansBanqueParNomPrenom(banque, nom, prenom)) {
			RechercheClientServices recherche = new RechercheClientServices();
			return recherche.rechercheClientDansBanqueParNomPrenom(banque, nom, prenom);
		}
		return null;
	}

	/**
	 * Permet de saisir l'id du client a recherche et le retourne s'il est trouve.
	 * Retourne null sinon.
	 * 
	 * @param banque
	 * @return
	 */
	public Client rechercheClientDansBanqueParSaisieID(Banque banque) {
		System.out.println("Veuillez entrer id client");
		// Faire un getter pour le scanner de la banque
		String idClient = Saisie.saisieIdClient(banque.getIn());

		ControleExistenceClient cec = new ControleExistenceClient();
		if (cec.existenceClientDansBanqueParID(banque, idClient)) {
			RechercheClientServices recherche = new RechercheClientServices();
			return recherche.rechercheClientDansBanqueParID(banque, idClient);
		}
		return null;
	}

	
	/**
	 * Demande le conseiller qui s'occupe de ce client et l'id du client et retourne
	 * le client recherche. Retourne null si le client n'est pas contenu dans la
	 * liste du de clients du conseiller.
	 * 
	 * @param conseiller
	 * @param idClient
	 * @return
	 */
	public Client rechercheClientDansConseillerParID(Conseiller conseiller, String idClient) {
		return conseiller.getListeClients().get(idClient);
	}

	/**
	 * Demande le conseiller qui s'occupe du client recherche et le nom et le prenom
	 * du client recherche et retourne le client trouve. Retourne null si le client
	 * n'est pas contenu dans la liste des clients du conseiller.
	 * 
	 * @param conseiller
	 * @param nom
	 * @param prenom
	 * @return
	 */
	public Client rechercheClientDansConseillerParNomPrenom(Conseiller conseiller, String nom, String prenom) {
		for (Entry<String, Client> client : conseiller.getListeClients().entrySet()) {
			if (client.getValue().getNom().equals(nom) && client.getValue().getPrenom().equals(prenom)) {
				return client.getValue();
			}
		}
		return null;
	}


	/**
	 * Recherche un client par son id dans toutes les listes de conseillers de
	 * l'agence passee en parametre et le retourne s'il le trouve. Retourne null si
	 * le client n'est pas trouve.
	 * 
	 * @param agence
	 * @param idClient
	 * @return
	 */
	public Client rechercheClientDansAgenceParID(Agence agence, String idClient) {
		for (Entry<String, Conseiller> conseiller : agence.getListeConseiller().entrySet()) {
			ControleExistenceClient cec = new ControleExistenceClient();
			if (cec.existenceClientDansConseillerParID(conseiller.getValue(), idClient)) {
				return rechercheClientDansConseillerParID(conseiller.getValue(), idClient);
			}
		}
		return null;
	}

	/**
	 * Recherche un client par son nom et son prenom dans toutes les listes de
	 * conseillers de l'agence passee en parametre et le retourne s'il le trouve.
	 * Retourne null si le client n'est pas trouve.
	 * 
	 * @param agence
	 * @param nom
	 * @param prenom
	 * @return
	 */
	public Client rechercheClientDansAgenceParNomPrenom(Agence agence, String nom, String prenom) {
		for (Entry<String, Conseiller> conseiller : agence.getListeConseiller().entrySet()) {
			ControleExistenceClient cec = new ControleExistenceClient();
			if (cec.existenceClientDansConseillerParNomPrenom(conseiller.getValue(), nom, prenom)) {
				return rechercheClientDansConseillerParNomPrenom(conseiller.getValue(), nom, prenom);
			}
		}
		return null;
	}

	/**
	 * Recherche un client par son id dans toutes les listes de conseillers de
	 * toutes les agences de la banque passee en parametre et le retourne s'il le
	 * trouve. Retourne null si le client n'est pas trouve.
	 * 
	 * @param banque
	 * @param idClient
	 * @return
	 */
	public Client rechercheClientDansBanqueParID(Banque banque, String idClient) {
		for (Entry<String, Agence> agence : banque.getListeAgences().entrySet()) {
			ControleExistenceClient cec = new ControleExistenceClient();
			if (cec.existenceClientDansAgenceParID(agence.getValue(), idClient)) {
				return rechercheClientDansAgenceParID(agence.getValue(), idClient);
			}
		}
		return null;
	}

	/**
	 * Recherche un client par son nom et son prenom dans toutes les listes de
	 * conseillers de toutes les agences de la banque passee en parametre et le
	 * retourne s'il le trouve. Retourne null si le client n'est pas trouve.
	 * 
	 * @param banque
	 * @param nom
	 * @param prenom
	 * @return
	 */
	public Client rechercheClientDansBanqueParNomPrenom(Banque banque, String nom, String prenom) {
		for (Entry<String, Agence> agence : banque.getListeAgences().entrySet()) {
			ControleExistenceClient cec = new ControleExistenceClient();
			if (cec.existenceClientDansAgenceParNomPrenom(agence.getValue(), nom, prenom)) {
				return rechercheClientDansAgenceParNomPrenom(agence.getValue(), nom, prenom);
			}
		}
		return null;
	}

}
