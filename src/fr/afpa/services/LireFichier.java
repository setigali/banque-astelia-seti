package fr.afpa.services;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class LireFichier {

	/**
	 * Lit un fichier a partir de son chemin et retourne une liste de tableaux
	 * de chaines de caracteres pour chaque ligne du fichier Client ou Conseiller.
	 * @param cheminFichier
	 * @return
	 */
	public static ArrayList<String[]> lireFichierClientConseiller(String cheminFichier) {
		ArrayList<String[]> listeIDLoginMDP = new ArrayList<String[]>();
		try {
			FileReader fr = new FileReader(cheminFichier);
			BufferedReader br = new BufferedReader(fr);
			
			while (br.ready()) {
				String res = br.readLine();
				listeIDLoginMDP.add(traductionLigneIDLoginMDP(res));
			}
			br.close();
		} catch (FileNotFoundException e) {
			System.out.println("Le fichier '"+cheminFichier+"' n'existe pas.");
		} catch (IOException e) {
			System.out.println("Erreur d'entree/sortie.");
		}
		return listeIDLoginMDP;
	}
	
	/**
	 * Lit un fichier a partir de son chemin et retourne une liste de tableaux
	 * de chaines de caracteres pour chaque ligne du fichier Admin
	 * @param cheminFichier
	 * @return
	 */
	public static ArrayList<String[]> lireFichierAdmin(String cheminFichier) {
		return lireFichierClientConseiller(cheminFichier);
	}
	
	/**
	 * Retourne un tableau de chaine de caracteres qui représente une ligne de fichier Id, Login et Mot de passe.
	 * La premiere case est l'Id, la deuxieme le login et la derniere le mot de passe pour le client ou le conseiller.
	 * S'il s'agit d'une ligne du fichier Admin, la premiere case est le login et la derniere est le mot de passe.
	 * @param ligne
	 * @return
	 */
	private static String[] traductionLigneIDLoginMDP(String ligne) {
		return ligne.split(";");
	}
}
