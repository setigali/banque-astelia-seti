package fr.afpa.services;

import java.time.LocalDate;
import fr.afpa.controles.ControleExistenceClient;
import fr.afpa.controles.ControleExistenceCompte;
import fr.afpa.controles.ControleExistenceConseiller;
import fr.afpa.controles.Saisie;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.CompteBancaire;
import fr.afpa.entite.CompteCourant;
import fr.afpa.entite.Conseiller;
import fr.afpa.entite.LivretA;
import fr.afpa.entite.PEL;
import fr.afpa.ihm.Menus;

public class MenuConseiller {
	
	/**
	 * Permet de quitter le programme
	 * @param banque
	 * @param choix
	 * @param idConseiller
	 */
	public void quitter(Banque banque, String choix, String idConseiller) {
		if ("12".equals(choix)) {
			MenuAuthentification ma = new MenuAuthentification();
			ma.menuAuthentification(banque);
		}
		else if (!"13".equals(choix)) {
			menuConseiller(banque, idConseiller);
		}
		System.out.println("Au revoir !");
	}
	
	/**
	 * Permet de faire une modification sur les informations du client passe
	 * en parametre. La banque sur laquelle on travaille est elle aussi passsee en parametre.
	 * @param banque
	 * @param client
	 */
	public void modifierInfosClient(Banque banque, Client client) {
		if (client != null) {
			System.out.println("Quelle info souhaitez-vous changer parmi :");
			System.out.println("nom, prenom, date et email");
			String typeInfo = Saisie.saisieVide(banque.getIn());
			System.out.println("Veuillez saisir la nouvelle information");
			switch (typeInfo) {
				case "prenom" : client.setPrenom(Saisie.saisieVide(banque.getIn()));
								break;
				case "date" : client.setDateDeNaissance(ServiceGeneral.formatDate(Saisie.saisieDate(banque.getIn())));
								break;
				case "email" : client.setEmail(Saisie.saisieEmail(banque.getIn()));
								break;
				case "nom" : client.setNom(Saisie.saisieVide(banque.getIn()));
								break;
				default : {System.out.println("Mauvais type !");
							modifierInfosClient(banque, client);
							}
			}
			System.out.println("Les informations ont bien ete modifiees.");
		}
		else {
			System.out.println("Le client n'existe pas.");
		}
	}
	
	/**
	 * Cree un nouveau compte bancaire pour le client passe en parametre dans la banque elle aussi
	 * passee en parametre.
	 * @param banque
	 * @param client
	 */
	public void creerCompteBancaire(Banque banque, Client client) {
		ClientServices cs = new ClientServices();
		if (cs.calculNombreComptes(client) < 3) {
			String numCompte = Aleatoire.chiffresAleatoire(11);
			
			// Verifier que le numero de compte n'existe pas.
			ControleExistenceCompte cec = new ControleExistenceCompte();
			while (cec.controleExistenceCompteBancaireDansBanque(banque, numCompte)) {
				numCompte = Aleatoire.chiffresAleatoire(11);
			}
			CompteBancaire cb = new CompteBancaire();
			System.out.println("Quel type de compte le client souhaite ouvrir ? (pel, compteCourant ou livretA)");
			String type = Saisie.saisieVide(banque.getIn());
			if ("compteCourant".equals(type) || !client.getTableauDeCompteBancaire().isEmpty()) {
				switch (type) {
					case "livretA" : cb = new LivretA(numCompte, 0f, false, 25f, false);
										break;
					case "pel" : cb = new PEL(numCompte, 0f, false, false);
									break;
					default : cb = new CompteCourant(numCompte, 0f, false);
				}
				client.getTableauDeCompteBancaire().put(numCompte, cb);
				System.out.println("Le nouveau compte a ete cree !");
				System.out.println("Son numero est : "+cb.getNumeroDeCompte());
			}
			else {
				System.out.println("Il faut d'abord creer un compte courant avant d'ouvrir un compte epargne !");
			}
		}
		else {
			System.out.println("Vous avez deja trois comptes actifs !");
		}
	}
	
	/**
	 * Cree un client et l'ajoute dans la liste de clients du conseiller passe en parametre
	 * (Le conseiller doit appartenir a la banque passee en parametre.)
	 * @param banque
	 * @param conseiller
	 */
	public void creerClient(Banque banque, Conseiller conseiller) {
		System.out.println("Veuillez entrer le nom du nouveau client");
		String nom = Saisie.saisieVide(banque.getIn());
		System.out.println("Veuillez entrer le prenom du nouveau client");
		String prenom = Saisie.saisieVide(banque.getIn());
		ControleExistenceClient cec = new ControleExistenceClient();
		if (!cec.existenceClientDansBanqueParNomPrenom(banque, nom, prenom)) {
			System.out.println("Veuillez entrer votre date de naissance");
			LocalDate date = ServiceGeneral.formatDate(Saisie.saisieDate(banque.getIn()));
			System.out.println("Veuillez entrer l'email du nouveau client");
			String mail = Saisie.saisieEmail(banque.getIn());
			Client client = new Client(nom, prenom, date, mail, false);
			
			// Ajouter le client dans la liste du conseiller qui s'est logge.
			conseiller.getListeClients().put(client.getIdClient(), client);
			
			// Ajouter le login et le mot de passe choisis dans le fichier de logins client en cryptant le mdp
			System.out.println("Veuillez choisir un login pour le nouveau client");
			String login = Saisie.saisieVide(banque.getIn());
			System.out.println("Veuillez choisir un mot de passe");
			String mdp = Saisie.saisieVide(banque.getIn());
			EcritureFichier.ecrireIdLoginMDPDansFichier("ressources\\Fichier_Clients.txt", client.getIdClient(), login, mdp);
			System.out.println("Le nouveau client a ete cree !");
			System.out.println("Son identifiant est : "+client.getIdClient());
		}
		else {
			System.out.println("Il y a deja un client avec ce nom et ce prenom dans notre banque !");
		}
	}


	/**
	 * Permet de changer la domiciliation d'un client
	 * @param banque
	 */
	public void changerDomiciliation(Banque banque) {
		RechercheClientServices recherche = new RechercheClientServices();
		
		// Quel client change de domiciliation ?
		// On recupere le client via la fonction rechercheClientParIDou.....
		Client client = recherche.rechercheClientParIDOuNom(banque);
		// On recupere l'id de l'ancien conseiller
		System.out.println("Veuillez entrer l'id de l'ancien conseiller");
		// Creer fonction de saisie pour idConseiller
		String idAncienConseiller = Saisie.saisieIdConseiller(banque.getIn());	
			
		// Verification existence conseiller
		System.out.println("Veuillez entrer l'id du nouveau conseiller");
		String idNouveauConseiller = Saisie.saisieIdConseiller(banque.getIn());
		ControleExistenceConseiller cec = new ControleExistenceConseiller();
		if (cec.existenceConseillerDansBanqueParId(banque, idNouveauConseiller)) {
				
			// Ajout du client dans la liste de son nouveau conseiller 
			RechercheConseillerServices rcs = new RechercheConseillerServices();
			rcs.rechercheConseillerDansBanqueParId(banque, idNouveauConseiller).getListeClients().put(client.getIdClient(), client);
			
			// Suppression du client dans la liste de son ancien conseiller
			rcs.rechercheConseillerDansBanqueParId(banque, idAncienConseiller).getListeClients().put(client.getIdClient(), client);
			System.out.println("Le changement a ete effectue.");
		}
		else {
			System.out.println("Cette agence n'a pas de conseiller a assigne au client");
		}		
	}
	
	
	/**
	 * Menu du conseiller avec l'id du conseiller et la banque passes en parametre 
	 * @param banque
	 * @param idConseiller
	 */
	public void menuConseiller(Banque banque, String idConseiller) {
		Menus.menuConseiller("conseiller");
		System.out.println("Que souhaitez-vous faire ?");
		String choix = Saisie.saisieVide(banque.getIn());
		
		MenuClient mc = new MenuClient();
		switch (choix) {
			case "1" :;
			case "2" :;
			case "3" :;
			case "4" :;
			case "5" :;
			case "6" :;
			case "7" : {
					RechercheClientServices rcs  = new RechercheClientServices();
					mc.fonctionsMenuClient(rcs.rechercheClientParIDOuNom(banque), banque);
					break;}
			
			case "8" : {
					RechercheClientServices rcs  = new RechercheClientServices();
					creerCompteBancaire(banque, rcs.rechercheClientParIDOuNom(banque));
					break;}
			
			case "9" : {
					RechercheConseillerServices rcs  = new RechercheConseillerServices();
					creerClient(banque, rcs.rechercheConseillerDansBanqueParId(banque, idConseiller));
					break;}
			
			case "10" : changerDomiciliation(banque);
						break;
						
			case "11" : {
					RechercheClientServices rcs  = new RechercheClientServices();
					modifierInfosClient(banque, rcs.rechercheClientParIDOuNom(banque));
					break;}
						
		}
		quitter(banque, choix, idConseiller);
	}
	
	

}
