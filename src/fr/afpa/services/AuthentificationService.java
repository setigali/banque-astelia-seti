package fr.afpa.services;

import java.util.Scanner;

import fr.afpa.controles.Saisie;
import fr.afpa.ihm.Menus;

public class AuthentificationService {
	
	
	/**
	 * Choix du profil
	 * @param scanner
	 * @return
	 */
	public String choixProfil(Scanner scanner) {
		Menus.menuChoixProfil();
		
		Saisie saisie = new Saisie();
		String profil = saisie.profilSaisie(scanner);
		
		return profil;
	}
	
	
	
}

