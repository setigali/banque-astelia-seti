package fr.afpa.services;

import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import fr.afpa.controles.ControleExistenceAgence;
import fr.afpa.controles.Saisie;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.CompteBancaire;
import fr.afpa.entite.Conseiller;
import fr.afpa.entite.LivretA;
import fr.afpa.entite.PEL;

public class MenuAdmin {
	/**
	 * creation du codeAgence aleatoirement
	 * 
	 * @param banque
	 * @return code agence
	 */
	public String codeAgenceAleatoire(Banque banque) {
		String code;

		code = Aleatoire.chiffresAleatoire(3);

		ControleExistenceAgence cea = new ControleExistenceAgence();

		while (cea.existenceAgenceDansBanqueParId(banque, code)) {
			code = Aleatoire.chiffresAleatoire(3);
		}
		return code;
	}

	/**
	 * creation de l'identifiant conseiller aleatoirement
	 * 
	 * @param banque
	 * @return identifiant conseiller
	 */
	public String idConseillerAleatoire(Banque banque) {
		String id;

		id = "ICO" + Aleatoire.chiffresAleatoire(7);

		ControleExistenceAgence cea = new ControleExistenceAgence();

		while (cea.existenceAgenceDansBanqueParId(banque, id)) {
			id = "ICO" + Aleatoire.chiffresAleatoire(7);
		}
		return id;
	}

	/**
	 * creation d'une nouvelle agence
	 * 
	 * @param banque
	 */
	public void creerAgence(Banque banque) {
		String codeAgenceNouvelleAgence;
		String nomNouvelleAgence;
		String adresseNouvelleAgence;

		codeAgenceNouvelleAgence = codeAgenceAleatoire(banque);

		System.out.println("Veuillez saisir le nom de la nouvelle agence");
		nomNouvelleAgence = Saisie.saisieVide(banque.getIn());

		System.out.println("Veuillez saisir l'adresse de la nouvelle agence");
		adresseNouvelleAgence = Saisie.saisieVidePhrase(banque.getIn());

		Agence nouvelleAgence = new Agence(codeAgenceNouvelleAgence, nomNouvelleAgence, adresseNouvelleAgence);

		banque.getListeAgences().put(nouvelleAgence.getCodeAgence(), nouvelleAgence);

		System.out.println("La nouvelle agence est cr�e ! Voici les d�tails : " + codeAgenceNouvelleAgence);

	}

	/**
	 * creation d'un nouveau conseiller
	 * 
	 * @param banque
	 */
	public void creerConseiller(Banque banque) {
		String idNouveauConseiller;
		String nomNouveauConseiller;
		String prenomNouveauConseiller;
		String mailNouveauConseiller;

		idNouveauConseiller = idConseillerAleatoire(banque);

		System.out.println("Veuillez saisir le nom du nouveau conseiller");
		nomNouveauConseiller = Saisie.saisieVide(banque.getIn());

		System.out.println("Veuillez saisir le prenom du nouveau conseiller");
		prenomNouveauConseiller = Saisie.saisieVide(banque.getIn());

		System.out.println("Veuillez saisir le mail du nouveau conseiller");
		mailNouveauConseiller = Saisie.saisieEmail(banque.getIn());

		Conseiller nouveauConseiller = new Conseiller(idNouveauConseiller, nomNouveauConseiller,
				prenomNouveauConseiller, mailNouveauConseiller);
		System.out.println("Dans quelle agence voulez-vous mettre le conseiller?");
		String nomAgenceNouveauConseiller = Saisie.saisieVide(banque.getIn());

		RechercheAgenceService rea = new RechercheAgenceService();

		rea.rechercheAgenceDansBanqueParId(banque, nomAgenceNouveauConseiller);

		banque.getListeAgences().get(nomAgenceNouveauConseiller).getListeConseiller().put(idNouveauConseiller,
				nouveauConseiller);

		// ajoute dans le fichier dedie l'id, le login et le mot de passe

		System.out.println("Veuillez choisir un login pour le nouveau conseiller (CO + 4 chiffres)");
		String login = Saisie.saisieVide(banque.getIn());
		System.out.println("Veuillez choisir un mot de passe pour le nouveau conseiller");
		String mdp = Saisie.saisieVide(banque.getIn());
		EcritureFichier.ecrireIdLoginMDPDansFichier("ressources\\Fichier_Conseillers.txt",
				nouveauConseiller.getIdConseiller(), login, mdp);
		System.out.println("Le nouveau conseiller a ete cree ! Voici son identifiant : " + idNouveauConseiller);
	}

	/**
	 * desactive un compte
	 * 
	 * @param banque
	 * @return le compte desactive
	 */
	public boolean desactiverUnCompte(Banque banque) {

		System.out.println("Veuillez saisir le numero du compte a desactiver ");
		String numeroCompteAdesactiver = Saisie.saisieNumeroDCompte(banque.getIn());
		RechercheCompteServices reCompte = new RechercheCompteServices();
		//reCompte.rechercheCompteDansBanqueParID(banque, numeroCompteAdesactiver).getNumeroDeCompte();

		//CompteBancaire compte = reCompte.rechercheCompteDansBanqueParID(banque, numeroCompteAdesactiver);

		// on ne peut bloquer que le PEL et le livretA
		if(reCompte.rechercheCompteDansBanqueParID(banque, numeroCompteAdesactiver) instanceof PEL) {
			((PEL)reCompte.rechercheCompteDansBanqueParID(banque, numeroCompteAdesactiver)).setBloque(true);
		}
		
		else if(reCompte.rechercheCompteDansBanqueParID(banque, numeroCompteAdesactiver) instanceof LivretA) {
			((LivretA) reCompte.rechercheCompteDansBanqueParID(banque, numeroCompteAdesactiver)).setCompteBloque(true);
			
			
		}

		return true;
	}

	/**
	 * desactive un client
	 * 
	 * @param banque
	 * @param client
	 * @return le client desactiv�
	 */
	public boolean desactiverClient(Banque banque, Client client) {
		String idClientAdesactiver;
		System.out.println("Veuillez saisir l'identifiant du client que vous souhaiter desactiver");
		idClientAdesactiver = Saisie.saisieIdClient(banque.getIn());
		RechercheClientServices rec = new RechercheClientServices();
		rec.rechercheClientDansBanqueParID(banque, idClientAdesactiver).setClientBloque(true);

		System.out.println("Le client " + idClientAdesactiver + "a �t� d�sactiv� !");
		return true;
	}

	/**
	 * Permet de quitter le programme
	 * 
	 * @param banque
	 * @param choix
	 * @param idConseiller
	 */
	public void quitter(Banque banque, String choix) {
		if ("16".equals(choix)) {
			MenuAuthentification ma = new MenuAuthentification();
			ma.menuAuthentification(banque);
		} else if (!"17".equals(choix)) {
			fonctionsMenuAdmin(banque);
		}
		System.out.println("Au revoir !");
	}

	public void fonctionsMenuAdmin(Banque banque) {
		Scanner in = new Scanner(System.in);
		String option;
		System.out.println("Quelle operation souhaitez-vous effectuer?");

		option = in.nextLine();

		
		switch (option) {
		case "1":
			;

		case "2":
			;

		case "3":
			;

		case "4":
			;

		case "5":
			;

		case "6":
			;

		case "7":
			;

		case "8":
			;

		case "9":
			;

		case "10":
			;

		case "11":
			menuConseiller(banque);
			break;

		case "12": // creer conseiller
			creerConseiller(banque);
			break;
		case "13": // creer agence
			creerAgence(banque);
			break;
		case "14": // desactiver un compte
			desactiverUnCompte(banque);
			break;
		case "15": // desactiver client

		case "16":
			quitter(banque, option);
			break;
		// case quitter et default
		default:
			System.out.println("Veuillez saisir un caractere valide.");
			break;

		}

	}

	
	private void menuConseiller(Banque banque) {
		System.out.println("Entrer l'id du conseiller");
		String id = Saisie.saisieIdConseiller(banque.getIn());
		List<String[]> listeInfoConseillers = LireFichier.lireFichierClientConseiller("ressources\\Fichier_Conseillers.txt");
		int i=0;
		while (i<listeInfoConseillers.size() && !id.equals(listeInfoConseillers.get(i)[0])) {
			i++;
		}
		if (i < listeInfoConseillers.size()) {
			MenuConseiller mco = new MenuConseiller();
			mco.menuConseiller(banque, id);
		}
	}
}
