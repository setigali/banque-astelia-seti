package fr.afpa.services;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import fr.afpa.controles.Controle;
import fr.afpa.controles.ControleCompte;
import fr.afpa.controles.ControleExistenceClient;
import fr.afpa.controles.ControleExistenceCompte;
import fr.afpa.controles.Saisie;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.CompteBancaire;
import fr.afpa.entite.CompteCourant;
import fr.afpa.entite.LivretA;
import fr.afpa.entite.Operation;
import fr.afpa.entite.PEL;
import fr.afpa.ihm.InfosClient;

public class MenuClient {

	public void afficherListeDesOperations(Banque banque) {

		String idcb;
		LocalDate debut;
		LocalDate fin;

		System.out.println("Veuillez saisir le numero de compte");
		idcb = Saisie.saisieNumeroDCompte(banque.getIn());
		System.out.println("Veuillez saisir la date de debut");
		debut = ServiceGeneral.formatDate(Saisie.saisieDate(banque.getIn()));
		System.out.println("Veuillez saisir la fin");
		fin = ServiceGeneral.formatDate(Saisie.saisieDate(banque.getIn()));
		RechercheCompteServices rechercheCompte = new RechercheCompteServices();
		CompteBancaire compte = rechercheCompte.rechercheCompteDansBanqueParID(banque, idcb);
		InfosClient.afficheListeOperationsCompteBancaire(compte, debut, fin);

	}

	public boolean virement(Banque banque) {
		alimenterUnCompte(banque);
		debiterUnCompte(banque);
		System.out.println("Virement effectu� !");

		return true;
	}

	public boolean alimenterUnCompte(Banque banque) {
		System.out.println("Veuillez saisir le compte a crediter");
		String idCompteBancaire = Saisie.saisieNumeroDCompte(banque.getIn());
		RechercheCompteServices rechercheCompteAC = new RechercheCompteServices();
		CompteBancaire compteAlimenter = rechercheCompteAC.rechercheCompteDansBanqueParID(banque, idCompteBancaire);
		// float copie = compteAlimenter.getSolde();
		System.out.println("Veuillez saisir le montant");
		float montant = Float.parseFloat(Saisie.saisieFloat(banque.getIn()));
		ControleExistenceCompte cec = new ControleExistenceCompte();
		if (cec.controleExistenceCompteBancaireDansBanque(banque, idCompteBancaire)) {
			rechercheCompteAC.rechercheCompteDansBanqueParID(banque, idCompteBancaire)
					.setSolde(compteAlimenter.getSolde() + montant);

			// ajouter � la liste des operations
			Operation e = new Operation(LocalDate.now(), true, montant, idCompteBancaire);
			rechercheCompteAC.rechercheCompteDansBanqueParID(banque, idCompteBancaire).getListeDesOperations().add(e);
			System.out.println(" Op�ration effectu�e ! ");
		}

		return true;
	}

	public boolean debiterUnCompte(Banque banque) {

		System.out.println("Veuillez saisir le compte a debiter");
		String idCompteBancaire = Saisie.saisieNumeroDCompte(banque.getIn());
		RechercheCompteServices rechercheCompteAD = new RechercheCompteServices();
		CompteBancaire compteAdebiter = rechercheCompteAD.rechercheCompteDansBanqueParID(banque, idCompteBancaire);

//		float compte = compteAdebiter.getSolde();
		System.out.println("Veuillez saisir le montant");
		float montant = Float.parseFloat(Saisie.saisieFloat(banque.getIn()));
		ControleExistenceCompte cec = new ControleExistenceCompte();

		if (cec.controleExistenceCompteBancaireDansBanque(banque, idCompteBancaire)) {
			rechercheCompteAD.rechercheCompteDansBanqueParID(banque, idCompteBancaire)
					.setSolde(compteAdebiter.getSolde() + montant);

			if (compteAdebiter != null && compteAdebiter.getSolde() - montant >= 0) {

				compteAdebiter.setSolde(compteAdebiter.getSolde() - montant);

				// ajouter l'operation � la liste des operations
				Operation e = new Operation(LocalDate.now(), true, montant, idCompteBancaire);
				rechercheCompteAD.rechercheCompteDansBanqueParID(banque, idCompteBancaire).getListeDesOperations()
						.add(e);
				System.out.println(" Op�ration effectu�e ! ");
			}

		}
		return true;
	}

	/**
	 * Permet de faire un virement entre deux comptes.
	 * 
	 * @param banque
	 */
	public void virerArgent(Banque banque) {
		System.out.println("Saisir le numero du compte a debiter.");
		String numCptADebiter = Saisie.saisieNumeroDCompte(banque.getIn());
		System.out.println("Saisir le numero du compte a debiter.");
		String numCptACrediter = Saisie.saisieNumeroDCompte(banque.getIn());
		System.out.println("Saisir le montant.");
		float montant = Float.parseFloat(Saisie.saisieFloat(banque.getIn()));

		ControleExistenceCompte cec = new ControleExistenceCompte();
		if (cec.controleExistenceCompteBancaireDansBanque(banque, numCptACrediter)
				&& cec.controleExistenceCompteBancaireDansBanque(banque, numCptADebiter)) {
			RechercheCompteServices rcs = new RechercheCompteServices();
			if (montant >= 0) {
				debit_credit(banque, false);
				debit_credit(banque, true);
			} else {
				System.out.println("Le montant est negatif !");
			}
		} else {
			System.out.println("L'un des comptes n'existe pas.");
		}
	}

	/**
	 * Permet de crediter ou de debiter un compte
	 * 
	 * @param banque : la banque qui contient le compte
	 * @param bool   : credit si true, debit si false
	 */
	public void debit_credit(Banque banque, boolean bool) {
		System.out.println("Saisir le numero de compte.");
		String numCompte = Saisie.saisieNumeroDCompte(banque.getIn());
		System.out.println("Saisir le montant.");
		float montant = Float.parseFloat(Saisie.saisieFloat(banque.getIn()));
		ControleExistenceCompte cec = new ControleExistenceCompte();
		if (cec.controleExistenceCompteBancaireDansBanque(banque, numCompte)) {
			RechercheCompteServices rcs = new RechercheCompteServices();
			if (bool) {
				operationCredit(rcs.rechercheCompteDansBanqueParID(banque, numCompte), montant);
			} else {
				operationDebit(rcs.rechercheCompteDansBanqueParID(banque, numCompte), montant);
			}
		} else {
			System.out.println("Le compte n'existe pas.");
		}
	}

	/**
	 * Permet de proceder au credit sur un compte.
	 * 
	 * @param compte  : compte a crediter
	 * @param montant : montant a crediter
	 */
	private void operationCredit(CompteBancaire compte, float montant) {
		if (montant < 0) {
			System.out.println("Le montant est negatif !");
		} else if ((compte instanceof CompteCourant)
				|| (compte instanceof LivretA && !((LivretA) compte).isCompteBloque())
				|| (compte instanceof PEL && !((PEL) compte).isBloque())) {
			compte.setSolde(compte.getSolde() - montant);
			Operation operation = new Operation(LocalDate.now(), true, montant, compte.getNumeroDeCompte());
			compte.getListeDesOperations().add(operation);
		} else {
			System.out.println("Le compte a crediter n'est pas active !");
		}
	}

	/**
	 * Permet de proceder au debit sur un compte.
	 * 
	 * @param compte  : compte a debiter
	 * @param montant : montant a debiter
	 */
	private void operationDebit(CompteBancaire compte, float montant) {
		if (compte.getSolde() < 0 && !compte.isDecouvert()) {
			System.out.println("Il n'y a pas assez d'argent sur le compte a debiter.");
		} else if (montant < 0) {
			System.out.println("Le montant entre est negatif !");
		} else if ((compte instanceof CompteCourant)
				|| (compte instanceof LivretA && !((LivretA) compte).isCompteBloque())
				|| (compte instanceof PEL && !((PEL) compte).isBloque())) {
			compte.setSolde(compte.getSolde() - montant);
			Operation operation = new Operation(LocalDate.now(), false, montant, compte.getNumeroDeCompte());
			compte.getListeDesOperations().add(operation);
		} else {
			System.out.println("Le compte a debiter n'est pas active !");
		}
	}

	public void imprimerReleveDeCompte(Banque banque) {
		LocalDate debut;
		LocalDate fin;
		String compte;

		System.out.println("Veuillez saisir votre compte bancaire");
		compte = Saisie.saisieNumeroDCompte(banque.getIn());

		RechercheCompteServices rec = new RechercheCompteServices();
		CompteBancaire cb = rec.rechercheCompteDansBanqueParID(banque, compte);

		System.out.println("Veuillez saisir la date de debut");
		debut = ServiceGeneral.formatDate(Saisie.saisieDate(banque.getIn()));

		System.out.println("Veuillez saisir la date de fin");
		fin = ServiceGeneral.formatDate(Saisie.saisieDate(banque.getIn()));

		InfosClient.afficheListeOperationsCompteBancaire(cb, debut, fin);

	}

	public void quitter(Banque banque, String choix, Client client) {
		if ("8".equals(choix)) {
			MenuAuthentification ma = new MenuAuthentification();
			ma.menuAuthentification(banque);
		} else if (!"9".equals(choix)) {
			fonctionsMenuClient(client, banque);
		}
		System.out.println("Au revoir !");
	}

	public void fonctionsMenuClient(Client client, Banque banque) {
		Scanner in = new Scanner(System.in);
		System.out.println("Quelle operation souhatez-vous effectuer?");
		String option = "";
		option = in.nextLine();
		switch (option) {

		case "1":
			InfosClient.afficheClient(client);
			break;

		case "2":
			InfosClient.afficheListeDeComptes(client);
			break;

		case "3":
			afficherListeDesOperations(banque);
			break;

		case "4":
			// virement(banque);
			virerArgent(banque);
			break;

		case "5":
			imprimerReleveDeCompte(banque);
			break;

		case "6":
			// alimenterUnCompte(banque);
			debit_credit(banque, true);
			break;

		case "7":
			// debiterUnCompte(banque);
			debit_credit(banque, false);
			break;

		}
		quitter(banque, option, client);
	}
}