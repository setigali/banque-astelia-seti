package fr.afpa.services;

import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;

public class RechercheAgenceService {

	/**
	 * Demande la banque dans la quelle on cherche l'agence
	 * ainsi que l'id de l'agence et retourne l'agence recherchée.
	 * @param banque
	 * @param id
	 * @return
	 */
	public Agence rechercheAgenceDansBanqueParId(Banque banque, String id) {
		return banque.getListeAgences().get(id);
	}
	
}
