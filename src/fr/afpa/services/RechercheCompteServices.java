package fr.afpa.services;

import java.util.Map.Entry;

import fr.afpa.controles.ControleExistenceCompte;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.CompteBancaire;
import fr.afpa.entite.Conseiller;

public class RechercheCompteServices {

	public CompteBancaire rechercheCompteDansClientParID(Client client, String numeroDeCompte) {

		return client.getTableauDeCompteBancaire().get(numeroDeCompte);

	}

	public CompteBancaire rechercheCompteDansConseillerParID(Conseiller conseiller, String numeroDeCompte) {

		for (Entry<String, Client> client : conseiller.getListeClients().entrySet()) {
			ControleExistenceCompte cec = new ControleExistenceCompte();
			if (cec.controleExistenceCompteBancaireDansListeDeCompteDuClient(client.getValue(), numeroDeCompte)) {
				return rechercheCompteDansClientParID(client.getValue(), numeroDeCompte);
			}

		}

		return null;
	}

	public CompteBancaire rechercheCompteDansAgenceparID(Agence agence, String numeroDeCompte) {

		for (Entry<String, Conseiller> conseiller : agence.getListeConseiller().entrySet()) {
			ControleExistenceCompte cec = new ControleExistenceCompte();
			if (cec.controleExistenceCompteBancaireDansConseiller(conseiller.getValue(), numeroDeCompte)) {
				return rechercheCompteDansConseillerParID(conseiller.getValue(), numeroDeCompte);
			}

		}
		return null;
	}

	public CompteBancaire rechercheCompteDansBanqueParID(Banque banque, String numeroDeCompte) {

		for (Entry<String, Agence> agence : banque.getListeAgences().entrySet()) {
			ControleExistenceCompte cec = new ControleExistenceCompte();
			if (cec.controleExistenceCompteBancaireDansAgence(agence.getValue(), numeroDeCompte)) {

				return rechercheCompteDansAgenceparID(agence.getValue(), numeroDeCompte);
			}

		}

		return null;
	}
}
