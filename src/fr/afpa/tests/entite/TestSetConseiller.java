package fr.afpa.tests.entite;

import java.util.HashMap;

import org.junit.jupiter.api.Test;

import fr.afpa.entite.Client;
import fr.afpa.entite.Conseiller;
import junit.framework.TestCase;

public class TestSetConseiller extends TestCase {
	
	private Conseiller conseiller;
	private final String idConseiller = "ICO1234567";
	private final String nom = "Le perdant";
	private final String prenom = "Fayak";
	private final String mail = "fayak.leperdant@gmal.com";
	
	
	public void setUp() throws Exception {
		super.setUp();
		conseiller = new Conseiller(idConseiller, nom, prenom, mail);
	}
	

	@Test
	public void testSetIDConseiller() {
		String idTemp = "ICO9876543";
		assertEquals("Probleme au demarrage", idConseiller, conseiller.getIdConseiller());
		conseiller.setIdConseiller(idTemp);
		assertEquals("Identifiants pas egaux", idTemp, conseiller.getIdConseiller());
		
	}
	
	@Test
	public void testSetNom() {
		String nomTemp = "toto";
		assertEquals("Probleme au demarrage", nom, conseiller.getNom());
		conseiller.setNom(nomTemp);
		assertEquals("Noms pas egaux", nomTemp, conseiller.getNom());
	}
	
	@Test
	public void testSetPrenom() {
		String prenomTemp = "toto";
		assertEquals("Probleme au demarrage", prenom, conseiller.getPrenom());
		conseiller.setPrenom(prenomTemp);
		assertEquals("Prenoms pas egaux", prenomTemp, conseiller.getPrenom());
	}
	
	@Test
	public void testSetMail() {
		String mailTemp = "toto@gmal.org";
		assertEquals("Probleme au demarrage", mail, conseiller.getMail());
		conseiller.setMail(mailTemp);
		assertEquals("Mails pas egaux", mailTemp, conseiller.getMail());
	}
	
	@Test
	public void testSetListeClients() {
		HashMap<String, Client> listeClient = new HashMap<String, Client>();
		listeClient.put("AM123456", new Client());
		listeClient.put("SG156780", new Client());
		assertTrue("Soucis au demarrage", conseiller.getListeClients().isEmpty());
		conseiller.setListeClients(listeClient);
		assertEquals("Soucis au niveau du setListeClients", listeClient, conseiller.getListeClients());
	}

	
	public void tearDown() throws Exception {
		super.tearDown();
		conseiller = null;
	}


}
