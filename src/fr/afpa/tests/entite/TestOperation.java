package fr.afpa.tests.entite;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import fr.afpa.entite.Operation;
import junit.framework.TestCase;

public class TestOperation extends TestCase {

	private Operation operation;

	@Test
	public void testOperation() {
		LocalDate date = LocalDate.of(2019, 05, 18);
		boolean nature = true;
		float valeur = 45.18f;
		String numCompte = "1234567890";
		operation = new Operation(date, nature, valeur, numCompte);
		assertEquals("La date n'est pas valide", date, operation.getDate());
		assertEquals("Probleme au niveau de la nature", nature, operation.isNature());
		assertEquals("La valeur est 0", valeur, operation.getValeur());
		assertEquals("Numero de compte null", numCompte, operation.getNumeroCompte());
	}
	
	public void tearDown() throws Exception {
		super.tearDown();
		operation = null;
	}

	

}
