package fr.afpa.tests.entite;

import org.junit.jupiter.api.Test;

import fr.afpa.entite.CompteCourant;
import junit.framework.TestCase;

public class TestCompteCourant extends TestCase {

	private CompteCourant compte;
	
	public void setUp() throws Exception {
		super.setUp();
		compte = new CompteCourant("1234567890", 10.58f, false);
	}
	
	@Test
	public void testFrais() {
		assertEquals("Erreur au niveau des frais de compte", 25f, compte.getFraisDeTenueDeCompte());
	}
	
	@Test
	public void testNom() {
		assertEquals("Erreur au niveau du nom du compte", CompteCourant.getNom(), compte.getNom());
	}
	
	public void tearDown() throws Exception {
		super.tearDown();
	}

	

}
