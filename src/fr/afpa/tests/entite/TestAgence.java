package fr.afpa.tests.entite;

import org.junit.jupiter.api.Test;

import fr.afpa.entite.Agence;
import junit.framework.TestCase;

public class TestAgence extends TestCase {

	private Agence agence;

	@Test
	public void testAgenceNonNulle() {
		String codeAgence = "234";
		String nom = "ToutRisques";
		String adresse = "24 rue des pres";
		agence = new Agence(codeAgence, nom, adresse);
		assertEquals("Code agence nulle", codeAgence, agence.getCodeAgence());
		assertEquals("Nom null", nom, agence.getNom());
		assertEquals("Adresse nulle", adresse, agence.getAdresse());
		assertTrue("Liste non vide a la creation !", agence.getListeConseiller().isEmpty());
	}

	
	public void tearDown() throws Exception {
		super.tearDown();
		agence = null;
	}
	
}
