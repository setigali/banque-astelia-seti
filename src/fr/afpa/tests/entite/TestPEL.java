package fr.afpa.tests.entite;

import org.junit.jupiter.api.Test;

import fr.afpa.entite.PEL;
import fr.afpa.services.PELServices;
import junit.framework.TestCase;

public class TestPEL extends TestCase {

	private PEL compte;
	private final float solde = 10.58f;
	
	public void setUp() throws Exception {
		super.setUp();
		compte = new PEL("1234567890", solde, false, false);
	}


	@Test
	public void testCalculFrais() {
		assertEquals("Erreur au niveau des frais de compte", PELServices.calculFrais(solde), compte.getFraisDeTenueDeCompte());
	}
	
	@Test
	public void testNom() {
		assertEquals("Erreur au niveau du nom", PEL.getNom(), compte.getNom());
	}
	
	public void tearDown() throws Exception {
		super.tearDown();
	}


}
