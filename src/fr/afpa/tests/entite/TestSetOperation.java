package fr.afpa.tests.entite;

import java.time.LocalDate;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.afpa.entite.Operation;
import junit.framework.TestCase;

public class TestSetOperation extends TestCase {

	private Operation operation;
	private final LocalDate date = LocalDate.of(2019, 05, 18);
	private final boolean nature = true;
	private final float valeur = 45.18f;
	private final String numCompte = "1234567890";
	
	
	public void setUp() throws Exception {
		super.setUp();
		operation = new Operation(date, nature, valeur, numCompte);
	}

	@Test
	public void testSetDate() {
		LocalDate dateTemp = LocalDate.now();
		assertEquals("Probleme de demarrage", date, operation.getDate());
		operation.setDate(dateTemp);
		assertEquals("Probleme setDate", dateTemp, operation.getDate());
	}
	
	@Test
	public void testSetNature() {
		boolean natureTemp = false;
		assertTrue("Probleme de demarrage", operation.isNature());
		operation.setNature(natureTemp);
		assertFalse("Probleme setNature", operation.isNature());
	}
	
	@Test
	public void testSetValeur() {
		float valeurTemp = 5.6f;
		assertEquals("Probleme de demarrage", valeur, operation.getValeur());
		operation.setValeur(valeurTemp);
		assertEquals("Probleme setValeur", valeurTemp, operation.getValeur());
	}
	
	@Test
	public void testSetNumCompte() {
		String numTemp = "truc";
		assertEquals("Probleme de demarrage", numCompte, operation.getNumeroCompte());
		operation.setNumeroCompte(numTemp);
		assertEquals("Probleme setNumCompte", numTemp, operation.getNumeroCompte());
	}
	
	
	protected void tearDown() throws Exception {
		super.tearDown();
		operation = null;
	}

	

}
