package fr.afpa.tests.entite;

import fr.afpa.tests.TestControle;
import fr.afpa.tests.TestCryptage;
import fr.afpa.tests.TestRechercheAgence;
import fr.afpa.tests.TestRechercheClient;
import fr.afpa.tests.TestSaisie;
import junit.extensions.ActiveTestSuite;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

public class TestSuiteAppli extends TestCase {

	public TestSuiteAppli(String name) {
		super(name);
	}

	
	public static TestSuite suite() {
		TestSuite suite = new ActiveTestSuite();
		suite.addTest(new TestSuite(TestAgence.class));
		suite.addTest(new TestSuite(TestSetAgence.class));
		suite.addTest(new TestSuite(TestConseiller.class));
		suite.addTest(new TestSuite(TestSetConseiller.class));
		suite.addTest(new TestSuite(TestCompteCourant.class));
		suite.addTest(new TestSuite(TestPEL.class));
		suite.addTest(new TestSuite(TestOperation.class));
		suite.addTest(new TestSuite(TestSetOperation.class));
		
		suite.addTest(new TestSuite(TestSaisie.class));
		suite.addTest(new TestSuite(TestControle.class));
		suite.addTest(new TestSuite(TestCryptage.class));
		suite.addTest(new TestSuite(TestRechercheClient.class));
		suite.addTest(new TestSuite(TestRechercheAgence.class));
		return suite;
	}
	
	
	public static void main(String[] args) {
		TestRunner.run(suite());

	}

}
