package fr.afpa.tests.entite;

import java.util.HashMap;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import fr.afpa.entite.Agence;
import fr.afpa.entite.Conseiller;
import junit.framework.TestCase;

public class TestSetAgence extends TestCase {

	private Agence agence;
	private final String code = "123";
	private final String nom = "nom";
	private final String adresse = "adresse";
	
	@BeforeEach
	public void setUp() throws Exception {
		super.setUp();
		agence = new Agence(code, nom, adresse);
	}


	@Test
	public void testSetCodeAgence() {
		String codeAgence = "234";
		assertEquals("Soucis au demarrage", code, agence.getCodeAgence());
		agence.setCodeAgence(codeAgence);
		assertEquals("Soucis au niveau du setCodeAgence", codeAgence, agence.getCodeAgence());
	}
	
	@Test
	public void testSetNom() {
		String nomTemp = "truc";
		assertEquals("Soucis au demarrage", nom, agence.getNom());
		agence.setNom(nomTemp);
		assertEquals("Soucis au niveau du setNom", nomTemp, agence.getNom());
	}
	
	@Test
	public void testSetAdresse() {
		String adresseTemp = "bidule";
		assertEquals("Soucis au demarrage", adresse, agence.getAdresse());
		agence.setAdresse(adresseTemp);
		assertEquals("Soucis au niveau du setAdresse", adresseTemp, agence.getAdresse());
	}
	
	@Test
	public void testSetListeConseiller() {
		HashMap<String, Conseiller> listeConseiller = new HashMap<String, Conseiller>();
		listeConseiller.put("CO1234", new Conseiller());
		listeConseiller.put("CO5678", new Conseiller());
		assertTrue("Soucis au demarrage", agence.getListeConseiller().isEmpty());
		agence.setListeConseiller(listeConseiller);
		assertEquals("Soucis au niveau du setListeConseiller", listeConseiller, agence.getListeConseiller());
	}

	
	@AfterEach
	public void tearDown() throws Exception {
		super.tearDown();
		agence = null;
	}
	
}
