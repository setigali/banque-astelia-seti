package fr.afpa.tests.entite;

import org.junit.jupiter.api.Test;

import fr.afpa.entite.Conseiller;
import junit.framework.TestCase;

public class TestConseiller extends TestCase {
	
	private Conseiller conseiller;

	@Test
	public void testConseillerNonNull() {
		String idConseiller = "234";
		String nom = "Le perdant";
		String prenom = "Fayak";
		String mail = "fayak.leperdant@gmal.com";
		conseiller = new Conseiller(idConseiller, nom, prenom, mail);
		assertEquals("Id conseiller null", idConseiller, conseiller.getIdConseiller());
		assertEquals("Nom null", nom, conseiller.getNom());
		assertEquals("Prenom null", prenom, conseiller.getPrenom());
		assertEquals("Mail null", mail, conseiller.getMail());
		assertTrue("Liste non vide a la creation !", conseiller.getListeClients().isEmpty());
	}

	
	public void tearDown() throws Exception {
		super.tearDown();
		conseiller = null;
	}
	
}
