package fr.afpa.tests;



import org.junit.Test;

import fr.afpa.controles.Controle;
import junit.framework.TestCase;

public class TestControle extends TestCase {

	public void setUp() throws Exception {
		super.setUp();
	}

	@Test
	public void testNumeroCompteBancaire() {

		boolean resultat = Controle.numeroCompteBancaire("01234567892");
		boolean resultat1 = Controle.numeroCompteBancaire("0123654");
		assertTrue("Code invalide", resultat);
		assertFalse("Code faux", resultat1);

	}

	@Test
	public void testIdClient() {

		boolean resultat = Controle.idClient("FR123456");
		boolean resultat1 = Controle.idClient("F123456");
		
		assertTrue("Identifiant invalide", resultat);
		assertFalse("Identifiant faux", resultat1);
	}

	@Test
	public void testCodeAgence() {

		boolean resultat = Controle.codeAgence("123");
		boolean resultat1 = Controle.codeAgence("12");
		
		assertTrue("Code agence invalide", resultat);
		assertFalse("Code agence faux", resultat1);
	}

	@Test
	public void testLoginClient() {
		
		boolean resultat = Controle.loginClient("0123456789");
		boolean resultat1 = Controle.loginClient("012345");
		
		assertTrue("Login client invalide", resultat);
		assertFalse("Login client faux", resultat1);

	}

	@Test
	public void testLoginConseiller() {
		boolean resultat = Controle.loginConseiller("CO1234");
		boolean resultat1 = Controle.loginConseiller("1234");
		
		assertTrue("Login Conseiller invalide", resultat);
		assertFalse("Login Conseiller faux", resultat1);
	}

	@Test
	public void testLoginAdmin() {
		boolean resultat = Controle.loginAdmin("ADM12");
		boolean resultat1 = Controle.loginAdmin("ADM");
		
		assertTrue("Login admin invalide", resultat);
		assertFalse("Login admin faux", resultat1);
	}

	@Test
	public void testDates() {
		boolean resultat = Controle.formatDate("12/11/2019");
		boolean resultat1 = Controle.formatDate("12/11-2019");
		
		assertTrue("Format date invalide", resultat);
		assertFalse("Format date faux",  resultat1);

	}

	@Test
	public void testEntier() {
		boolean resultat = Controle.formatEntier("20");
		boolean resultat1 = Controle.formatEntier("");
		
		assertTrue("Format entier invalide",resultat);
		assertFalse("Format entier faux",resultat1);
	}

	@Test
	public void testFloat() {
		boolean resultat = Controle.formatFloat("12.50");
		boolean resultat1 = Controle.formatFloat("");
		
		assertTrue("Format float saisi invalide", resultat);
		assertFalse("Format float saisi faux", resultat1);

	}

	@Test
	public void testEmail() {
		boolean resultat = Controle.formatEmail("astelia@afpa.com");
		boolean resultat1 = Controle.formatEmail("astelia@afpa.");
		
		assertTrue("Format email invalide",resultat);
		assertFalse("Format email faux",resultat1);
	}

	@Test
	public void testChaineVide() {
		boolean resultat = Controle.refusChaineVide(" ");
		boolean resultat1 = Controle.refusChaineVide("");
		
		assertTrue("Saisie invalide", resultat);
		assertFalse("Saisie faux", resultat1);

	}
	
	@Test
	public void testIdConseiller() {
		boolean resultat = Controle.formatIdConseiller("ICO1234567");
		boolean resultat1 = Controle.formatIdConseiller("1234567");
		
		assertTrue("Id conseiller invalide", resultat);
		assertFalse("Id conseiller faux", resultat1);
	}
	
	@Test
	public void testProfil() {
		boolean resultat = Controle.profil("conseiller");
		boolean resultat2 = Controle.profil("client");
		boolean resultat3 = Controle.profil("admin");
		
		assertEquals("Profil conseiller invalide", true, resultat);
		assertEquals("Profil client invalide", true, resultat2);
		assertEquals("Profil admin invalide", true, resultat3);
	}
	
	@Test
	public void testChoix() {
		boolean resultat = Controle.idOuNom("id");
		boolean resultat2 = Controle.idOuNom("nom");
		
		assertEquals("Choix id invalide", true, resultat);
		assertEquals("Choix nom invalide", true, resultat2);
	}
}
