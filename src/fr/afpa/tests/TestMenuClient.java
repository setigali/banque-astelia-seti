package fr.afpa.tests;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import org.junit.Test;

import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.CompteBancaire;
import fr.afpa.entite.Operation;
import fr.afpa.services.MenuClient;
import junit.framework.TestCase;

public class TestMenuClient extends TestCase {

	public void setUp() throws Exception {
		super.setUp();

	}

	@Test
	public void testAfficherOperation() {
		// String idcb = "0123456789";
		CompteBancaire compte = new CompteBancaire("0123456789", 1258.50f, true, 10.50f);
		// compte.setNumeroDeCompte(idcb);
		LocalDate date = LocalDate.now();
		Client client = new Client("Gali", "Seti", date, "GaliSeti@afpa.com", false);
		client.getTableauDeCompteBancaire().put("seti", compte);

		ArrayList<Operation> resultat = compte.getListeDesOperations();

		assertNotNull("Pas d'operation", resultat);

		String debut = "10/09/2019";
		String fin = "15/09/2019";

		/*
		 * Scanner scanner ; Banque banque = new Banque();
		 * 
		 * // Conseiller conseiller = new
		 * Conseiller("ICO1234567","nom","prenom","20 rue du Luxembourg");
		 * 
		 * 
		 * 
		 * assertEquals("Pas d'operations",
		 * banque.getListeAgences().get(banque).getListeConseiller().get(banque).
		 * getListeClients().get(banque).getTableauDeCompteBancaire().get(idcb).
		 * getListeDesOperations());
		 */

	}

	@Test
	public void testVirement() {
		MenuClient menuc = new MenuClient();
		CompteBancaire compte = new CompteBancaire("0123456789", 1258.50f, true, 10.50f);
		Scanner scanner = new Scanner(System.in);
		Banque banque = new Banque(scanner);

		boolean resultat = menuc.virement(banque);
		assertTrue("Virement effectu�", true);
		ArrayList<Operation> resultat2 = compte.getListeDesOperations();
		menuc.afficherListeDesOperations(banque);
		assertNotNull("Pas d'op�ration", resultat2);

	}

}
