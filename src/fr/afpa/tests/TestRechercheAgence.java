package fr.afpa.tests;

import java.util.HashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;

import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.services.RechercheAgenceService;
import junit.framework.TestCase;

public class TestRechercheAgence extends TestCase {

	private Banque banque;
	private Agence agence1;
	private Agence agence2;
	
	
	public void setUp() throws Exception {
		super.setUp();
		
		banque = new Banque();		
		agence1 = new Agence("123", "Tout Risques", "32 rue des Rosiers");		
		
		Map<String, Agence> listeAgences = new HashMap<String, Agence>();
		listeAgences.put("123", agence1);
		banque.setListeAgences((HashMap)listeAgences);
	}
	
	
	@Test
	public void testRechercheAgencetDansBanqueParIDTrouve() {
		RechercheAgenceService ras = new RechercheAgenceService();
		assertSame("Pas la meme reference ", agence1, ras.rechercheAgenceDansBanqueParId(banque, agence1.getCodeAgence()));
	}
	
	@Test
	public void testRechercheAgenceDansBanqueParIDNonTrouve() {
		RechercheAgenceService ras = new RechercheAgenceService();
		assertNull("Agence non null", ras.rechercheAgenceDansBanqueParId(banque, agence2.getCodeAgence()));
	}
	
}
