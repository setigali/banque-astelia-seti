package fr.afpa.tests;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.junit.jupiter.api.Test;

import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Conseiller;
import fr.afpa.services.RechercheClientServices;
import junit.framework.TestCase;

public class TestRechercheClient extends TestCase {

	private Banque banque;
	private Agence agence;
	private Conseiller conseiller;
	private Client client1;
	private Client client2;
	
	
	public void setUp() throws Exception {
		super.setUp();
		
		banque = new Banque();		
		agence = new Agence();		
		conseiller = new Conseiller();
		client1 = new Client("Wayne", "Bruce", LocalDate.of(1993, 02, 28), "bruce@gmal.com", false);
		client2 = new Client("Al Ghul", "Talia", LocalDate.of(1995, 04, 1), "talia@gmal.com", false);
		
		Map<String, Client> listeClients = new HashMap<String, Client>();
		listeClients.put(client1.getIdClient(), client1);
		conseiller.setListeClients((HashMap) listeClients);
		
		Map<String, Conseiller> listeConseillers = new HashMap<String, Conseiller>();
		listeConseillers.put("ICO1234567", conseiller);
		agence.setListeConseiller((HashMap) listeConseillers);
		
		Map<String, Agence> listeAgences = new HashMap<String, Agence>();
		listeAgences.put("123", agence);
		banque.setListeAgences((HashMap)listeAgences);
	}
	
	@Test
	public void testRechercheClientDansConseillerParIDTrouve() {
		RechercheClientServices rcs = new RechercheClientServices();
		assertSame("Pas la meme reference dans conseiller", client1, rcs.rechercheClientDansConseillerParID(conseiller, client1.getIdClient()));
	}
	
	@Test
	public void testRechercheClientDansConseillerParIDNonTrouve() {
		RechercheClientServices rcs = new RechercheClientServices();
		assertNull("Client non null", rcs.rechercheClientDansConseillerParID(conseiller, client2.getIdClient()));
	}
	
	@Test
	public void testRechercheClientDansAgenceParIDTrouve() {
		RechercheClientServices rcs = new RechercheClientServices();
		assertSame("Pas la meme reference agence", client1, rcs.rechercheClientDansAgenceParID(agence, client1.getIdClient()));
	}
	
	@Test
	public void testRechercheClientDansAgenceParIDNonTrouve() {
		RechercheClientServices rcs = new RechercheClientServices();
		assertNull("Client non null", rcs.rechercheClientDansAgenceParID(agence, client2.getIdClient()));
	}
	
	@Test
	public void testRechercheClientDansBanqueParIDTrouve() {
		RechercheClientServices rcs = new RechercheClientServices();
		assertSame("Pas la meme reference ", client1, rcs.rechercheClientDansBanqueParID(banque, client1.getIdClient()));
	}
	
	@Test
	public void testRechercheClientDansBanqueParIDNonTrouve() {
		RechercheClientServices rcs = new RechercheClientServices();
		assertNull("Client non null", rcs.rechercheClientDansBanqueParID(banque, client2.getIdClient()));
	}
	
	@Test
	public void testRechercheClientDansConseillerParNomPrenomTrouve() {
		RechercheClientServices rcs = new RechercheClientServices();
		assertSame("Pas la meme reference dans conseiller", client1, rcs.rechercheClientDansConseillerParNomPrenom(conseiller, client1.getNom(), client1.getPrenom()));
	}
	
	@Test
	public void testRechercheClientDansConseillerParNomPrenomNonTrouve() {
		RechercheClientServices rcs = new RechercheClientServices();
		assertNull("Client non null", rcs.rechercheClientDansConseillerParNomPrenom(conseiller, client2.getNom(), client2.getPrenom()));
	}
	
	@Test
	public void testRechercheClientDansAgenceParNomPrenomTrouve() {
		RechercheClientServices rcs = new RechercheClientServices();
		assertSame("Pas la meme reference agence", client1, rcs.rechercheClientDansAgenceParNomPrenom(agence, client1.getNom(), client1.getPrenom()));
	}
	
	@Test
	public void testRechercheClientDansAgenceParNomPrenomNonTrouve() {
		RechercheClientServices rcs = new RechercheClientServices();
		assertNull("Client non null", rcs.rechercheClientDansAgenceParNomPrenom(agence, client2.getNom(), client2.getPrenom()));
	}
	
	@Test
	public void testRechercheClientDansBanqueParNomPrenomTrouve() {
		RechercheClientServices rcs = new RechercheClientServices();
		assertSame("Pas la meme reference ", client1, rcs.rechercheClientDansBanqueParNomPrenom(banque, client1.getNom(), client1.getPrenom()));
	}
	
	@Test
	public void testRechercheClientDansBanqueParNomPrenomNonTrouve() {
		RechercheClientServices rcs = new RechercheClientServices();
		assertNull("Client non null", rcs.rechercheClientDansBanqueParNomPrenom(banque, client2.getNom(), client2.getPrenom()));
	}

	
	public void tearDown() throws Exception {
		super.tearDown();
		banque = null;
		agence = null;
		conseiller = null;
		client1 = null;
		client2 = null;
	}
}
