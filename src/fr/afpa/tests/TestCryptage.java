package fr.afpa.tests;

import org.junit.jupiter.api.Test;

import fr.afpa.services.CryptageServices;
import junit.framework.TestCase;

public class TestCryptage extends TestCase {

	@Test
	public void testCryptageDecryptage() {
		String resultat = "";
		
		String texte = "abc";
		resultat = CryptageServices.cryptage(texte);
		assertEquals("Probleme au niveau du cryptage", "hgf", resultat);
		resultat = CryptageServices.decryptage(resultat);
		assertEquals("Probleme au niveau du decryptage", texte, resultat);
	}

}
