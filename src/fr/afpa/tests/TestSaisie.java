package fr.afpa.tests;

import java.util.Scanner;

import org.junit.Test;
import fr.afpa.controles.Saisie;
import junit.framework.TestCase;

public class TestSaisie extends TestCase {
	

	
	public void setUp() throws Exception {
		super.setUp();
	}
	
	@Test
	public void testNumeroCompte() {
		
		String resultat = Saisie.saisieNumeroDCompte(new Scanner("0123456789"));
		
		assertEquals("Saisie numero invalide", resultat);
	}
	
	// test bizzarres dans cette classe 
	
}
