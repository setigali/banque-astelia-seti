package fr.afpa.testsServices;

import java.time.LocalDate;
import java.util.HashMap;
import fr.afpa.entite.Client;
import fr.afpa.entite.CompteBancaire;
import fr.afpa.ihm.InfosClient;

public class TestAffichageInfo {

	public static void main(String[] args) {
		
		HashMap<String, CompteBancaire> liste = new HashMap<String, CompteBancaire>();
		liste.put("5632fg", new CompteBancaire("5632fg", 1200.36f, false, 25));
		liste.put("5hdh132hd", new CompteBancaire("5hdh132hd", 16354.14f, false, 25));
		liste.put("561rgq2", new CompteBancaire("561rgq2", -2.69f, false, 25));
		
		Client client = new Client("Akbaraly", "Fayaz", LocalDate.of(1993, 02, 12), "bidule", true);
		client.setTableauDeCompteBancaire(liste);
		
		/*System.out.println(client.getIdClient());
		int nombreClientCree = 0;
		String test = Aleatoire.majusculesAleatoire(2)+ClientServices.autoIncrement(++nombreClientCree);
		System.out.println(test);*/
		
		InfosClient.afficheClient(client);
		System.out.println("\n");
		
		InfosClient.afficheListeDeComptes(client);
		System.out.println("\n");
		
		InfosClient.afficheFicheClient(client);

	}

}
