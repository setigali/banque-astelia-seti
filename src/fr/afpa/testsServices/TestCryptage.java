package fr.afpa.testsServices;

import fr.afpa.services.CryptageServices;

public class TestCryptage {

	public static void main(String[] args) {
		
		CryptageServices cryptage = new CryptageServices();
		
		String mot = "00MDA";
		System.out.println("Mot d'origine : "+mot);
		
		String motCrypte = cryptage.cryptage(mot);
		System.out.println("Mot crypte : "+motCrypte);
		
		String motDecrypte = cryptage.decryptage(motCrypte);
		System.out.println("Mot decrypte : "+motDecrypte);

	}

}
