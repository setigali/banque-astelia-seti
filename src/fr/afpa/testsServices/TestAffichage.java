package fr.afpa.testsServices;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import fr.afpa.entite.CompteBancaire;
import fr.afpa.entite.Operation;
import fr.afpa.ihm.InfosClient;

public class TestAffichage {

	public static void main(String[] args) {
		
		Operation op1 = new Operation(LocalDate.of(1989, 03, 24), true, 50f, "bidule");
		Operation op2 = new Operation(LocalDate.of(1990, 02, 26), false, 60f, "bidule");
		Operation op3 = new Operation(LocalDate.of(2003, 05, 19), true, 70f, "bidule");
		Operation op4 = new Operation(LocalDate.of(1994, 03, 24), false, 80f, "bidule");
		
		List<Operation> listeOperations = new ArrayList<Operation>();
		listeOperations.add(op1);
		listeOperations.add(op2);
		listeOperations.add(op3);
		listeOperations.add(op4);
		
		CompteBancaire cb = new CompteBancaire("bidule", 10.25f, false, 25);
		cb.getListeDesOperations().addAll(listeOperations);
		
		InfosClient infos = new InfosClient();
		infos.afficheListeOperationsCompteBancaire(cb, LocalDate.of(1990, 02, 26), LocalDate.of(2010, 12, 26));
	}

}
