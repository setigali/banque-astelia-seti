package fr.afpa.testsServices;

import java.time.LocalDate;
import java.util.Scanner;

import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.CompteBancaire;
import fr.afpa.entite.CompteCourant;
import fr.afpa.entite.Conseiller;
import fr.afpa.entite.PEL;
import fr.afpa.services.MenuAdmin;
import fr.afpa.services.RechercheClientServices;
import fr.afpa.services.RechercheCompteServices;
import fr.afpa.services.RechercheConseillerServices;

public class TestMenuAdmin {

	public static void main(String[] args) {
		
		// Creation
		Scanner scanner = new Scanner(System.in);
		String codeAgence = "123";
		String idBatman = "BW000001";
		String idJoker = "MJ000002";
		String idConseiller = "ICO1234567";
		String numCompte1 = "12345678910";
		String numCompte2 = "12345678916";
		
		Banque banque = new Banque(scanner);
		Agence agence = new Agence(codeAgence, "nom", "adresse");
		Conseiller conseiller = new Conseiller(idConseiller, "nom", "prenom", "mail");
		
		Client batman = new Client("Wayne", "Bruce", LocalDate.of(2019, 03, 24), "batman@gmal.com", false);
		Client joker = new Client("Joker", "Jack", LocalDate.of(2019, 02, 22), "joker@gmal.com", false);
		
		CompteBancaire cb1 = new CompteCourant(numCompte1, 80.6f, false);
		CompteBancaire cb2 = new PEL(numCompte2, 80.6f, false, false);		
		
		MenuAdmin ma = new MenuAdmin();
		
		// Test creer agence
		ma.creerAgence(banque);
		System.out.println("Est-ce que la banque a une agence ? "+!banque.getListeAgences().isEmpty());
		System.out.println();
		
		// Test creer conseiller
		ma.creerConseiller(banque);
		RechercheConseillerServices rcs = new RechercheConseillerServices();
		System.out.println();
		
		
		
		// Ajout dans les listes
		batman.getTableauDeCompteBancaire().put(numCompte1, cb1);
		batman.getTableauDeCompteBancaire().put(numCompte2, cb2);
		conseiller.getListeClients().put(idBatman, batman);
		conseiller.getListeClients().put(idJoker, joker);
		agence.getListeConseiller().put(idConseiller, conseiller);
		banque.getListeAgences().put(codeAgence, agence);
		
		// Test desactiver un compte
		ma.desactiverUnCompte(banque);
		RechercheCompteServices rCOs = new RechercheCompteServices();
		CompteBancaire compteDesactive = rCOs.rechercheCompteDansBanqueParID(banque, numCompte2);
		System.out.println("Le compte a ete bloque ? : "+((PEL)compteDesactive).isBloque());
		
		// Test desactiver un client
		ma.desactiverClient(banque, joker);
		RechercheClientServices rClients = new RechercheClientServices();
		joker = rClients.rechercheClientDansBanqueParID(banque, idJoker);
		System.out.println("Le joker a ete bloque ? : "+joker.isClientBloque());
	}

}
