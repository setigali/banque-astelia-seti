package fr.afpa.testsServices;

import java.util.Scanner;

import fr.afpa.entite.Agence;
import fr.afpa.entite.Banque;
import fr.afpa.entite.Client;
import fr.afpa.entite.Conseiller;
import fr.afpa.ihm.InfosClient;
import fr.afpa.services.MenuConseiller;
import fr.afpa.services.RechercheClientServices;

public class TestMenuConseiller {

	public static void main(String[] args) {
		
		// Creation
		Scanner scanner = new Scanner(System.in);
		String codeAgence = "123";
		String codeAgence2 = "456";
		String idConseiller = "ICO1234567";
		
		Banque banque = new Banque(scanner);
		Agence agence = new Agence(codeAgence, "nom", "adresse");
		Agence agence2 = new Agence(codeAgence2, "truc", "chouette");
		Conseiller conseiller = new Conseiller(idConseiller, "nom", "prenom", "mail");
		
		
		// Ajout dans les listes
		agence.getListeConseiller().put(idConseiller, conseiller);
		banque.getListeAgences().put(codeAgence, agence);
		banque.getListeAgences().put(codeAgence2, agence2);
		
		
		
		// Test creationClient
		MenuConseiller mc = new MenuConseiller();
		mc.creerClient(banque, conseiller);
		mc.creerClient(banque, conseiller);
		
		RechercheClientServices rcs = new RechercheClientServices();
		Client client1 = rcs.rechercheClientDansBanqueParNomPrenom(banque, "Wayne", "Bruce");
		Client client2 = rcs.rechercheClientDansBanqueParNomPrenom(banque, "Joker", "Jack");
		InfosClient.afficheFicheClient(client1);
		System.out.println();
		InfosClient.afficheFicheClient(client2);
		System.out.println();
		
		// Test creationCompte
		String nom = "Wayne";
		String prenom = "Bruce";
		mc.creerCompteBancaire(banque, rcs.rechercheClientDansBanqueParNomPrenom(banque, nom, prenom));
		Client client = rcs.rechercheClientDansBanqueParNomPrenom(banque, nom, prenom);
		InfosClient.afficheFicheClient(client);
		System.out.println();
		
		
		// Test changement domiciliation
		mc.changerDomiciliation(banque);
		
		
		
		// Test changement d'infos
		mc.modifierInfosClient(banque, client);
		InfosClient.afficheClient(client);
		
		// Test fonction menu conseiller
		mc.menuConseiller(banque, idConseiller);
		
	}
	
}
