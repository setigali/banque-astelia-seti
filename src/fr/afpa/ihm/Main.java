package fr.afpa.ihm;

import java.util.Scanner;

import fr.afpa.entite.Banque;
import fr.afpa.services.MenuAuthentification;

public class Main {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		// Creation de la banque
		Banque banque = new Banque(sc);
		
		// Lancement du menu d'authentification
		MenuAuthentification ma = new MenuAuthentification();
		ma.menuAuthentification(banque);
		
		sc.close();
		
	}

}
