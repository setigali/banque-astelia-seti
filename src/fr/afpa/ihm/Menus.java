package fr.afpa.ihm;

public class Menus {
	
	/**
	 * Affiche le menu du client si son profil passe en parametre est un client
	 * @param profil
	 */
	public static void menuClient(String profil) {
		if ("client".equals(profil)) {
			System.out.println("Menu client : ");
		}
		System.out.println("1 - Consulter informations clients");
		System.out.println("2 - Consulter liste de comptes");
		System.out.println("3 - Consulter operations de chaque compte");
		System.out.println("4 - Faire un virement");
		System.out.println("5 - Imprimer un releve de compte");
		System.out.println("6 - Alimenter un compte");
		System.out.println("7 - Retire argent");
		if ("client".equals(profil)) {
			System.out.println("8 - Retour Menu Authentification");
			System.out.println("9 - Quitter");
			System.out.println("________________________");
		}
	}
	
	/**
	 * Affiche le menu du conseiller si son profil passe en parametre est un conseiller
	 * @param profil
	 */
	public static void menuConseiller(String profil) {
		if ("conseiller".equals(profil)) {
			System.out.println("Menu conseiller : ");
		}
		menuClient(profil);
		System.out.println("8 - Creer un compte");
		System.out.println("9 - Creer un client");
		System.out.println("10 - Changer la domiciliation du compte");
		System.out.println("11 - Modifier les informations");
		if ("conseiller".equals(profil)) {
			System.out.println("12 - Retour Menu Authentification");
			System.out.println("13 - Quitter");
			System.out.println("________________________");
		}
	}
	
	/**
	 * Affiche le menu de l'admin si son profil passe en parametre est un admin
	 * @param profil
	 */
	public static void menuAdmin(String profil) {
		if ("admin".equals(profil)) {
			System.out.println("Menu admin : ");
		}
		menuConseiller(profil);
		System.out.println("12 - Creer un conseiller");
		System.out.println("13 - Creer une agence");
		System.out.println("14 - Desactiver un compte");
		System.out.println("15 - Desactiver un client");
		if ("admin".equals(profil)) {
			System.out.println("16 - Retour Menu Authentification");
			System.out.println("17 - Quitter");
			System.out.println("________________________");
		}
	}
	
	/**
	 * Affiche l'en-tete du menu d'authentification
	 */
	public static void menuAuthentification() {
		System.out.println("________________________");
		System.out.println();
		System.out.println("Menu d'authentification");
		System.out.println("________________________");
	}
	
	/**
	 * Affiche l'en-tete du choix du profil
	 */
	public static void menuChoixProfil() {
		System.out.println("________________________");
		System.out.println();
		System.out.println("Choix du profil");
		System.out.println("________________________");
	}
}
