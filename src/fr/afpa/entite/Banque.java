package fr.afpa.entite;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Banque  {
	private Scanner in ;
	private Map<String, Agence>listeAgences;

	
	public Banque(Scanner in) {
		super();
		this.listeAgences = new HashMap<String, Agence>();
		this.in=in;
		
	}


	public Banque() {
		super();
	}


	public Map<String, Agence> getListeAgences() {
		return listeAgences;
	}


	public void setListeAgences(HashMap<String, Agence> listeAgences) {
		this.listeAgences = listeAgences;
	}
	


	public void setIn(Scanner scanner) {
		this.in = scanner;
	}
	
	public Scanner getIn() {
		return in;
	}



	@Override
	public String toString() {
		return "Banque [getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}
	

}
