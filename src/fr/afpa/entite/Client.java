package fr.afpa.entite;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import fr.afpa.services.Aleatoire;
import fr.afpa.services.ClientServices;

public class Client implements Comparable {
	
	private static int nombreClientCree=0;
	
	private String idClient;
	private String nom;
	private String prenom;
	private LocalDate dateDeNaissance;
	private String email;
	private Map <String, CompteBancaire> tableauDeCompteBancaire;
	private boolean clientBloque;
	
	
	
	public Client(String nom, String prenom, LocalDate dateDeNaissance, String email,
			boolean clientBloque) {
		super();
        idClient= Aleatoire.majusculesAleatoire(2)+ClientServices.autoIncrement(++nombreClientCree);
		this.nom = nom;
		this.prenom = prenom;
		this.dateDeNaissance = dateDeNaissance;
		this.email = email;
		this.tableauDeCompteBancaire= new HashMap<String, CompteBancaire>();
		this.clientBloque = clientBloque;
	
		
		

	}

	public Client() {
		super();
	}

	public String getIdClient() {
		return idClient;
	}

	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public LocalDate getDateDeNaissance() {
		return dateDeNaissance;
	}

	public void setDateDeNaissance(LocalDate dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Map<String, CompteBancaire> getTableauDeCompteBancaire() {
		return tableauDeCompteBancaire;
	}

	public void setTableauDeCompteBancaire(HashMap<String, CompteBancaire> tableauDeCompteBancaire) {
		this.tableauDeCompteBancaire = tableauDeCompteBancaire;
	}

	public boolean isClientBloque() {
		return clientBloque;
	}

	public void setClientBloque(boolean clientBloque) {
		this.clientBloque = clientBloque;
	}


	public static int getNombreClientCree() {
		return nombreClientCree;
	}

	public static void setNombreClientCree(int nombreClientCree) {
		Client.nombreClientCree = nombreClientCree;
	}


	

	@Override
	public String toString() {
		return "Client [idClient=" + idClient + ", nom=" + nom + ", prenom=" + prenom + ", dateDeNaissance="
				+ dateDeNaissance + ", email=" + email + ", tableauDeCompteBancaire=" + tableauDeCompteBancaire
				+ ", clientBloque=" + clientBloque + ""  + "]";
	}

	@Override
	public int compareTo(Object obj) {
		Client clientCaste= ((Client)obj);
		int comparaisonClient = this.idClient.compareTo(clientCaste.idClient);
		return comparaisonClient;
	}

	@Override
	public boolean equals(Object obj) {
		Client clientCaste= ((Client)obj);
		boolean comparaisonClient = this.idClient.equals(clientCaste.idClient) && this.nom.contentEquals(clientCaste.nom) && this.prenom.equals(clientCaste.prenom) && this.dateDeNaissance.equals(clientCaste.dateDeNaissance) && this.email.contentEquals(clientCaste.email)&& this.tableauDeCompteBancaire.equals(clientCaste.tableauDeCompteBancaire) && this.clientBloque==(clientCaste.clientBloque) && this.idClient.equals(clientCaste.idClient);
		return comparaisonClient;
	}	
	
	
	
	
}

