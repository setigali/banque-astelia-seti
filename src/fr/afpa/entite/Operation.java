package fr.afpa.entite;

import java.time.LocalDate;

public class Operation implements Comparable {
	
	private LocalDate date;
	private boolean nature;
	private float valeur;
	private String numeroCompte;
	
	
	public Operation() {
		super();
	}
	
	public Operation(LocalDate date_, boolean nature_, float valeur_, String numero) {
		date = date_;
		nature = nature_;
		valeur = valeur_;
		numeroCompte = numero;
	}
	
	
	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date_) {
		date = date_;
	}

	public boolean isNature() {
		return nature;
	}

	public void setNature(boolean nature_) {
		nature = nature_;
	}

	public float getValeur() {
		return valeur;
	}

	public void setValeur(float valeur_) {
		valeur = valeur_;
	}

	public String getNumeroCompte() {
		return numeroCompte;
	}

	public void setNumeroCompte(String numeroCompte_) {
		numeroCompte = numeroCompte_;
	}

	
	/**
	 * On compare les operations par leur date.
	 */
	@Override
	public int compareTo(Object o) {
		return this.date.compareTo( ((Operation) o).date );
	}

	@Override
	public boolean equals(Object obj) {
		boolean date = this.date.isEqual( ((Operation) obj).date );
		boolean nature = this.nature == ((Operation) obj).nature;
		boolean valeur = this.valeur == ((Operation) obj).valeur;
		boolean numero = this.numeroCompte.equals( ((Operation) obj).numeroCompte );
		return date && nature && valeur && numero;
	}


	@Override
	public String toString() {
		return "Operation [date=" + date + ", nature=" + nature + ", valeur=" + valeur + ", numeroCompte="
				+ numeroCompte + "]";
	}

}
