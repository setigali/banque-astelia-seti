package fr.afpa.entite;

public class CompteCourant extends CompteBancaire {

	private static final String nom = "Compte courant";
	
	
	public CompteCourant() {
		super();
	}

	public CompteCourant(String numeroDeCompte, float solde, boolean decouvert) {
		super(numeroDeCompte,solde, decouvert, 25);
		
	}

	
	public static String getNom() {
		return nom;
	}

	
	@Override
	public String toString() {
		return "CompteCourant [nom="+nom+", toString()=" + super.toString() + "]";
	}

	
}
